//XHR Promise
request = obj => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(obj.method || "GET", obj.url);

        if (obj.headers) {
            Object.keys(obj.headers).forEach(key => {
                xhr.setRequestHeader(key, obj.headers[key]);
            });
        }
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response);
            } else {
                reject(xhr.statusText);
            }
        };
        xhr.onerror = () => reject(xhr.statusText);

        xhr.send(obj.body);
    });
};

const $address = $('#address');
let $info = $('#info');
let $payments = $('#top-payments');

function check(){
	alert('check');
	
}

/*check = () => {
	alert('check');
    $info.html('');
    let total = 0;
    let addressUrl = address.value;
    request({url: `/api?q=${addressUrl}`})
            .then(data => {
                const responseObject = JSON.parse(data)
				console.log(responseObject);
				let row =  `<thead>
                                        <tr>
                                            <th>Coin</th>
                                            <th>Blance</th>
                                            <th>USD</th>
                                        </tr>
                                    </thead>
                                    <tbody>`
				
				for(var prop in responseObject) {
					
					 
                 row+=                      `<tr>
                                            <td>${prop}</td>
                                            <td>${responseObject[prop][0]}</td>
                                            <td>$${responseObject[prop][1] * responseObject[prop][0]}</td>
                                        </tr>`
                                        
                                   

                
				}
				$info.html(`${row}</tbody>`);
                //total += balance * currency.unitPrice;
				$payments.style.visibility = 'visible';
            })
            .catch(error => {
            });
}*/

convertToBitcoinValue = (number) => {
    let numberString = String(number);
    if (numberString.length > 8) {
        const decimal = numberString.slice(numberString.length - 8, numberString.length)
        let main = numberString;
        main = main.slice(0, -8);
        main = main.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        main += `.${decimal}`;
        return main;
    } else {
        while (numberString.length < 8) {
            numberString = `0${numberString}`;
        }
        return `0.${numberString}`;
    }
}