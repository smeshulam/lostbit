// require express
const express = require('express');
const path    = require('path');
const getJSON = require('get-json');
require('node-import');
const b58 = imports('./js/bitcoinjs.min.js');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

// route for our homepage
router.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

router.get('/api', function(req, res) {
	var e ='';
  var walletAddress = req.param('q');
  res.setHeader('Content-Type', 'application/json');
  let jsonResponse = {};
    allCurrecies.forEach((currency) => {
		let addressUrl = walletAddress;
		if (currency.name === "bitcoinGold") {
            addressUrl = convert(walletAddress);
        }
		let addr = `${currency.addressUrl}${addressUrl}`;
		if(currency.name === "unitedBitcoin"){
			addr = `${currency.addressUrl}${addressUrl}?pageNo=1&pageSize=5`;
		}
		getJSON(addr, function(error, response) {
			if(response || response === 0 || response!= 'undefined' ){
				let balance;
				switch(currency.name) {
					case 'bitcoin':
						balance = convertToBitcoinValue(response);
					break;
					case 'bitcoinGold':
						balance = response.balance;
					break;
					case 'bitcoinCash':
						balance = convertToBitcoinValue(response.balanceSat);
					case 'superBitcoin':
						balance = convertToBitcoinValue(response.balanceSat);
					break;
					case 'bitCore':
						balance = response;
					break;
					case "segwit2x":
						balance = convertToBitcoinValue(response.balanceSat);
					break;
					case "unitedBitcoin":
					    if(response.suc && response.suc != false){
						balance = response.result.amount;
						}else{
							balance = 0;
						}
					break;	
					case 'clam':
						balance = response;
					break;
				}
				jsonResponse[currency.name] = [balance, currency.unitPrice];
				if(Object.keys(jsonResponse).length == 7){
					console.log(jsonResponse);
					res.send(jsonResponse);
				}
			}
	});
	});
});

function myCurrency (name, addressUrl, exchangeUrl) {
	    var _this = this;
        this.name = name;
        this.addressUrl = addressUrl;
        this.exchangeUrl = exchangeUrl;
        this.unitPrice = 0;
        getJSON(exchangeUrl, function(error, response){
			  if(!error){
				  if(response){
					switch(_this.name) {
						case 'bitcoinGold':
							_this.unitPrice = response.data.rate;
							break;
						case 'bitcoinCash':
							_this.unitPrice = response.data.bitstamp;
							break;
						case 'bitcoin':
							_this.unitPrice = response.USD.last;
						break;
						case "superBitcoin":
							_this.unitPrice = response[0].price_usd;
						break;
						case "bitCore":
							_this.unitPrice = response[0].price_usd;
						break;
						case "segwit2x":
							_this.unitPrice = response[0].price_usd;
						break;
						case "unitedBitcoin":
							_this.unitPrice = response[0].price_usd;
						break;
						case "clam":
							_this.unitPrice = response[0].price_usd;
						break;
					}
				  }
			  }else{
				  console.log(error);
			  }
		});
    }


// converts bitcoin address to bitcoin gold address	
convert = (bitcoinAddress) => {
	 try {
        decoded = bitcoin.address.fromBase58Check(bitcoinAddress);
        version = decoded['version'];

        switch (version) {
            case 0:
                message = 'BTG p2pkh address: ';
                version = 38;
                break;
            case 38:
                message = 'BTC p2pkh address: ';
                version = 0;
                break;
            case 5:
                message = 'BTG p2sh address: ';
                version = 23;
                break;
            case 23:
                message = 'BTC p2sh address: ';
                version = 5;
                break;
            default:
                throw 'unknown';
        }
        btgaddress = bitcoin.address.toBase58Check(decoded['hash'], version);
    } catch (err) {
        message = 'Please enter a valid address.';
        btgaddress = '';
    }
    return btgaddress;
}

check = (walletAddress) => {
   //let total = 0;
   let jsonResponse = {};
    allCurrecies.forEach((currency) => {
		let addressUrl = walletAddress;
		if (currency.name === "bitcoinGold") {
            addressUrl = convert(walletAddress);
        }
		let addr = `${currency.addressUrl}${addressUrl}`;
		if(currency.name === "unitedBitcoin"){
			addr = `${currency.addressUrl}${addressUrl}?pageNo=1&pageSize=5`;
		}
		
		getJSON(addr, function(error, response) {
			console.log(response,error);
		if(!error) {
			if(response || response === 0 || response!= 'undefined' || response != 'error'){
				let balance;
				switch(currency.name) {
					case 'bitcoin':
						balance = convertToBitcoinValue(response);
					break;
					case 'bitcoinGold':
						balance = response.balance;
					break;
					case 'bitcoinCash':
						balance = convertToBitcoinValue(response.balanceSat);
					case 'superBitcoin':
						balance = convertToBitcoinValue(response.balanceSat);
					break;
					case 'bitCore':
						balance = response;
					break;
					case "segwit2x":
						balance = convertToBitcoinValue(response.balanceSat);
					break;
					case "unitedBitcoin":
					console.log(response,error);
					    if(response.suc && response.suc != false){
						balance = response.result.amount;
						}else{
							balance = 0;
						}
					break;	
					case 'clam':
						balance = response;
					break;
				}
				jsonResponse[currency.name] = balance;
				console.log( Object.keys(jsonResponse).length);
				if(Object.keys(jsonResponse).length == 7){
					console.log(jsonResponse);
				}
				
			}
		}
	});
		
	});
}

convertToBitcoinValue = (number) => {
    let numberString = String(number);
    if (numberString.length > 8) {
        const decimal = numberString.slice(numberString.length - 8, numberString.length)
        let main = numberString;
        main = main.slice(0, -8);
        main += `.${decimal}`;
        return main;
    }else {
        while (numberString.length < 8) {
            numberString = `0${numberString}`;
        }
        return `0.${numberString}`;
    }
}


var allCurrecies = [];
allCurrecies.push(new myCurrency('bitcoin', 'https://blockchain.info/q/addressbalance/',
    'https://blockchain.info/ticker'));
allCurrecies.push(new myCurrency('bitcoinGold', 'https://btgexplorer.com/api/addr/',
    'https://explorer.bitcoingold.org/insight-api/currency'));
allCurrecies.push(new myCurrency("bitcoinCash", 'https://blockdozer.com/insight-api/addr/',
    'https://blockdozer.com/insight-api/currency'));	
allCurrecies.push(new myCurrency('superBitcoin','http://block.superbtc.org/insight-api/addr/',
	'https://api.coinmarketcap.com/v1/ticker/super-bitcoin/'));
allCurrecies.push(new myCurrency("bitCore", 'https://chainz.cryptoid.info/ric/api.dws?q=getbalance&a=',
    'https://api.coinmarketcap.com/v1/ticker/bitcore/'));
allCurrecies.push(new myCurrency("segwit2x", 'https://explorer.b2x-segwit.io/b2x-insight-api/addr/',
    'https://api.coinmarketcap.com/v1/ticker/segwit2x/'));
//allCurrecies.push(new myCurrency("unitedBitcoin", 'https://main.ub.com/portals/blockexplore/address/',
   // 'https://api.coinmarketcap.com/v1/ticker/united-bitcoin/'));
allCurrecies.push(new myCurrency("clam", 'http://khashier.com/chain/Clam/q/addressbalance/',
    'https://api.coinmarketcap.com/v1/ticker/clams/'));
	
	
console.log(check('1AC4fMwgY8j9onSbXEWeH6Zan8QGMSdmtA'));
	
	